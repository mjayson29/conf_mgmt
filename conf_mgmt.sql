-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2017 at 04:22 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `conf_mgmt`
--

-- --------------------------------------------------------

--
-- Table structure for table `cm_entries`
--

CREATE TABLE `cm_entries` (
  `e_id` int(11) NOT NULL,
  `e_title` varchar(250) NOT NULL,
  `e_name` varchar(50) NOT NULL,
  `e_school` varchar(250) NOT NULL,
  `e_email` varchar(250) NOT NULL,
  `e_attach` varchar(250) NOT NULL,
  `e_status` int(11) NOT NULL,
  `e_payment` varchar(250) NOT NULL,
  `e_approvedby` int(11) NOT NULL,
  `e_approved` datetime NOT NULL,
  `e_created` datetime NOT NULL,
  `e_category` int(11) NOT NULL,
  `e_presentation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cm_subject_category`
--

CREATE TABLE `cm_subject_category` (
  `sc_id` int(11) NOT NULL,
  `sc_desc` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cm_subject_category`
--

INSERT INTO `cm_subject_category` (`sc_id`, `sc_desc`) VALUES
(1, 'Arts, Humanities and Philosophy'),
(2, 'Business, Finance and Management'),
(3, 'Basic and Teacher Education'),
(4, 'Engineering and Technology'),
(5, 'Governance, Policy Studies and Social Sciences'),
(6, 'Institutional Research and Extension'),
(7, 'Information Technology, Computer and Computing'),
(8, 'Science and Mathematics');

-- --------------------------------------------------------

--
-- Table structure for table `cm_users`
--

CREATE TABLE `cm_users` (
  `u_id` int(11) NOT NULL,
  `u_uname` varchar(250) NOT NULL,
  `u_password` varchar(250) NOT NULL,
  `u_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cm_users`
--

INSERT INTO `cm_users` (`u_id`, `u_uname`, `u_password`, `u_name`) VALUES
(1, 'admin', '3fc0a7acf087f549ac2b266baf94b8b1', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cm_entries`
--
ALTER TABLE `cm_entries`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `cm_subject_category`
--
ALTER TABLE `cm_subject_category`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `cm_users`
--
ALTER TABLE `cm_users`
  ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cm_entries`
--
ALTER TABLE `cm_entries`
  MODIFY `e_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cm_subject_category`
--
ALTER TABLE `cm_subject_category`
  MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cm_users`
--
ALTER TABLE `cm_users`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
