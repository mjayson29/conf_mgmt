<?php 
global $active;
$active = 'home';
include 'header.php'; 
?>
<script type="text/javascript">
$(document).ready(function () {
    $('#myRoundabout').roundabout({
        shape: 'square',
        minScale: 0.93, // tiny!
        maxScale: 1, // tiny!
        easing: 'easeOutExpo',
        clickToFocus: 'true',
        focusBearing: '0',
        duration: 800,
        reflect: true
    });
});
</script>
          <div class="relative">
            <div id="gallery">
              <ul id="myRoundabout">
                <li><img src="images/img1.png" alt=""></li>
                <li><img src="images/img2.png" alt=""></li>
                <li><img src="images/img3.png" alt=""></li>
              </ul>
            </div>
          </div>
        </header>
        <!-- / header-->
        <!-- content -->
        <section id="content">
          <div class="line1">
            <div class="line2 wrapper">
              <div class="wrapper">
                <article class="col1">
                  <h2>Shortly About Us</h2>
                  <figure><img src="images/page1_img1.jpg" alt="" class="pad_bot1"></figure>
                  <div class="pad">
                    <p> This site will give you a schedule for the your abstract presentation.</p>
                  </div>
                </article>
                <article class="col3 pad_left1">
                  <h2>Recent Seminars</h2>
                  <div class="pad">
                    <div class="wrapper"> <span class="date"><span>12</span>june</span>
                      <p> <a href="#" class="link1">Sed ut perspiciatis</a><br>
                        This Free Website Template goes with two packages. With PSD source files and without. </p>
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
<div class="body4">
  <div class="main">
    <section id="content2">
      <div class="line2 wrapper">
        <div class="wrapper">
          <article class="col1">
            <h2>Our Mission</h2>
            <div class="wrapper">
              <figure class="left marg_right1"><img src="images/page1_img2.jpg" alt=""></figure>
              <p> <strong>At vero eos et accusamus iusto</strong> odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque <a href="#">corrupti quos dolores</a> et quas moles- tias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt. </p>
            </div>
          </article>
          <article class="col3 pad_left1">
            <h2>Upcoming Seminars</h2>
            <div class="pad">
              <div class="wrapper"> <span class="date"><span>12</span>jun</span>
                <p> <a href="#" class="link1">Sed ut perspiciatis</a><br>
                  Unde omnis iste natuerror tium doloremque laudany tium totamrem. </p>
              </div>
              <div class="wrapper"> <span class="date"><span>23</span>jun</span>
                <p> <a href="#" class="link1">Doperiam eaque ipsa </a><br>
                  Quae ab illo inventore veritatis et quasi archiecto emo enim ipsam. </p>
              </div>
            </div>
          </article>
        </div>
      </div>
    </section>
  </div>
</div>
<!-- / content -->
<?php include 'footer.php'; ?>