<?php 
global $active;
$active = 'about';
include 'header.php'; 
?>
        <!-- / header-->
        <!-- content -->
                <section id="content">
                    <div class="wrapper">
                        <h2>About Us</h2>
                        <div class="wrapper">
                            <figure class="left marg_right1"><img src="../images/page2_img1.jpg" alt=""></figure>
                            <p> <strong>The Technological University of the Philippines (TUP), specifically the University Research and Development Services (URDS) office handled the task in managing and organizing IHERF 2016 used manual procedures in collating abstracts which are collected for review and are included in the book of abstracts: </strong> </p>
                            <p>The TUP-URDS is also in-charge in scheduling the time and date of the research presentations. Using only MS word, MS Excel and MS Publisher they were able to accomplish the tasks but sought to have an automated system to aid in the assumed tasks. This brought the researches to conduct a study for the research forum that needed aid in collating the abstracts and in scheduling research presentations. A system that can generate daily progress reports, list research titles and authors, collate abstracts, sort the abstracts alphabetically per category, namely:</p>
                            <p>	Arts, Humanities and Philosophy; Business, Finance and Management; Basic and Teacher Education; Engineering and Technology; Governance, Policy Studies and Social Sciences; Institutional Research and Extension; Information Technology, Computer and Computing; Science and Mathematics<br></p>
                        </div>
                        <div class="pad">
                            <p>The research presentations are done simultaneously per category and every research presentation is schedule based on a pre-programmed timeline. The researchers conducted a study to develop a system for the PAASUC-NCR Research Consortium and TUP-URDS.</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="body4">
    <div class="main">
        <section id="content2">
            <div class="line2 wrapper">
                <div class="wrapper">
                    <article class="col1">
                        <h2>Our Mission</h2>
                        <div class="pad">
                            <p class="pad_bot2"> <strong> 2008 – Antese ipsum ppsum dolor snsectetuer adipisc ing elitesent vestibulum molestieser lacus. </strong> </p>
                            <p> Cum suada odirbi nunc odio gravida cursus nec luctusrem. Maecenas tristique orci ac sem. Duis ultricies pharetra magec accumsan malesuada orcinec sit. Aliquam scelerisque arcu eu nibh molestie aliquam. </p>
                            <p class="pad_bot2"> <strong> 2011 – Dolore ipsum ppsum dolor snsectetuer adipisc ing elitesent vestibulum. </strong> </p>
                            <p> Cum suada odirbi nunc odio gravida cursus nec luctusrem. Maecenas tristique orci ac sem. Duis ultricies pharetra m ipsum dolor sit amete. Aliquam scelerisque arcu eu nibh molestie nibh ullamcorper mollis id quis quamed at odio nisl. </p>
                        </div>
                    </article>
                    <article class="col2 pad_left1">
                        <h2>Our Vision</h2>
                        <div class="pad">
                            <p class="pad_bot1"> <strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </strong> </p>
                            <figure class="left marg_right1 marg_left1"><img src="images/page2_img2.jpg" alt=""></figure>
                            <p> Ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerc- tation ullamco laboris nisi ut aliquip ex ea commodo cons Duis aute irure dolor in rep- rehenderit in voluptate velit<br> esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Cum suada odirbi nunc odio gravida cursus nec luctusrem aecenas tristique. </p>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- / content -->
<?php include 'footer.php'; ?>