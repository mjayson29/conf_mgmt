<!DOCTYPE html>
<html lang="en">
<head>
<title>Conference Mangement | Register</title>
<meta charset="utf-8">
<link rel="stylesheet" href="foundation/css/foundation.min.css" type="text/css" media="all">
<link rel="stylesheet" href="DataTables-1.10.13/media/css/jquery.dataTables.min.css" type="text/css" media="all">
<!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" type="text/css" media="all"> -->
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<!-- <script type="text/javascript" src="js/jquery-1.5.2.js" ></script> -->
<script type="text/javascript" src="js/jquery-3.1.1.min.js" ></script>
<script type="text/javascript" src="DataTables-1.10.13/media/js/jquery.dataTables.min.js" ></script>
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" ></script> -->
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
<script type="text/javascript" src="js/Terminal_Dosis_300.font.js"></script>
<script type="text/javascript" src="js/atooltip.jquery.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<style type="text/css">.bg {behavior:url("js/PIE.htc")}</style>
<![endif]-->
</head>
<body id="page6">
<div class="body1">
    <div class="body2">
        <div class="body3">
            <div class="main">
        <!-- header -->
                <header>
                    <div class="wrapper">
                        <h1><a href="index.html" id="logo"></a></h1>
                        <form id="search" action="#" method="post">
                        </form>
                        <?php if(!isset($active)){ ?>
                        <nav>
                            <ul id="menu">
                                <li class="end"><a href="logout.php">Logout</a></li>
                            </ul>
                        </nav>
                        <?php } ?>
                    </div>
                </header>