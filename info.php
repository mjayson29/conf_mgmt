<?php 

include 'header2.php'; 
include 'dbcon.php'; 
include 'global.php'; 

session_start();
if(!isset($_SESSION['login_user'])){
    header("location: login.php");
}


if($_POST){
    $cid = $_POST['client'];
    if(isset($_POST['Accept'])){
        $presentation = $_POST['presentation'];
        $category = $_POST['category'];
        $status = 2;
        $message = "Accepted message"; // Accepted message
        $sql = "UPDATE CM_entries SET e_status = '".$status."', e_approvedby = 1, e_approved = '".date('Y-m-d H:i:s')."', e_category = '".$category."', e_presentation_date = '".date('Y-m-d H:i:s',strtotime($presentation))."' WHERE e_id = ".$cid;
    }elseif(isset($_POST['Reject'])){
        $status = 3;
        $message = "Rejection message"; // Rejection message
        $sql = "UPDATE CM_entries SET e_status = '".$status."', e_approvedby = 1, e_approved = '".date('Y-m-d H:i:s')."' WHERE e_id = ".$cid;
    }elseif(isset($_POST['Payment'])){
        $status = 5;
        $message = "Payment confirmation message http://localhost/cmanagement/upload.php?eid=".$cid; // Payment confirmation message
        $sql = "UPDATE CM_entries SET e_status = '".$status."' WHERE e_id = ".$cid;
    }
    // echo $sql;
    if($conn->query($sql)===TRUE){
        echo "Success!";
        $client = $cid;
        $entry = select_entry($client);
        $to = $entry['e_email']; // contact email
        $subject = "Conference Management System"; // email subject
        send_mail($message,$subject,$to); // uncomment this once email was configure
    }else{
        echo "An error occured while proccessing your request. Please try again.<br>".$conn->error;
    }
}

if(isset($_GET['id'])){
    $client = $_GET['id'];
}

if($client!=""){

    $sql = "SELECT * FROM CM_entries WHERE e_id = ".$client;
    $res = $conn->query($sql);
    if($res!=""){
        while ($row = $res->fetch_array(MYSQLI_ASSOC)) {
            switch ($row['e_status']) {
                case '1': $status = "New"; break;
                case '2': $status = "Accepted <label>(Waiting for Payment)</label>"; break;
                case '3': $status = "Rejected"; break;
                case '4': $status = "Paid"; break;
                case '5': $status = "Reserved"; break;
            }
            
?>
        <!-- / header-->
        <!-- content -->
                <section id="content">
                    <div class="wrapper">
                        <h2>Client Information</h2>
                        <form id="ContactForm" action="info.php" method="POST">
                            <div>
                                <input type="hidden" name="client" value="<?=$row['e_id']?>">
                                <div class="wrapper"> <span>Title:</span>
                                    <span><?=$row['e_title']?></span> 
                                </div>
                                <div class="wrapper"> <span>Presenter:</span>
                                    <span><?=$row['e_name']?></span>  
                                </div>
                                <div class="wrapper"> <span>Institution:</span>
                                    <span><?=$row['e_school']?></span>
                                </div>
                                <div class="wrapper"> <span>Email:</span>
                                    <span><?=$row['e_email']?></span>
                                </div>
                                <div class="wrapper"> <span>Abstract:</span>
                                    <span><a href="uploads/<?=$row['e_attach']?>"><?=$row['e_attach']?></a></span>
                                </div>
                                <div class="wrapper"> <span>Status:</span>
                                    <span><?=$status?></span>
                                </div>
                                <?php if($row['e_status']==1){ ?>
                                <div class="wrapper"> <span>Date of Presentation:</span>
                                    <span><input type="date" name="presentation" placeholder="MM/DD/YY"></span>
                                </div>
                                <div class="wrapper"> <span>Subject Category:</span>
                                    <select id="category" name="category">
                                        <option value=""> < Select Category > </option>
                                    <?php
                                    $sql1 = "SELECT * FROM CM_subject_category";
                                    $res1 = $conn->query($sql1);
                                    if($res1->num_rows>0){
                                        while ($row1 = $res1->fetch_array(MYSQLI_ASSOC)) {
                                    ?> 
                                        <option <?php echo ((isset($_GET['category']))?(($_GET['category']==$row1['sc_id'])?'selected':''):''); ?> value="<?=$row1['sc_id']?>"><?=$row1['sc_desc']?></option>
                                    <?php }
                                    }
                                     ?>
                                    </select>
                                </div>
                                <span>&nbsp;</span> 
                                <input type="submit" class="button" name="Reject" value="Reject">
                                <input type="submit" class="button" name="Accept" value="Accept">
                                <?php }else{ ?>
                                    <div class="wrapper"> <span>Date of Presentation:</span>
                                        <span><?=date('F d Y H:i:s',strtotime($row['e_presentation_date']))?></span>
                                    </div>
                                    <div class="wrapper"> <span>Subject Category:</span>
                                        <span><?=select_category($row['e_category'])?></span>
                                    </div>
                                <?php if($row['e_status']==4||$row['e_status']==5){ ?>
                                    <div class="wrapper"> <span>Proof of Payment:</span>
                                        <span><a href="uploads/<?=$row['e_payment']?>"><?=$row['e_payment']?></a></span>
                                    </div>
                                    <?php if($row['e_status']==4){ ?>
                                    <input type="submit" class="button" name="Payment" value="Confirm Payment">
                                <?php    }
                                    }
                                 } ?>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="body4">
    <div class="main">
        <section id="content2">
            <div class="line2 wrapper">
            </div>
        </section>
    </div>
</div>
<!-- / content -->
<?php 
        }
    }
}
include 'footer.php'; 

?>