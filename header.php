<!DOCTYPE html>
<html lang="en">
<head>
<title>Conference Management</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript" src="js/jquery-1.5.2.js" ></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
<script type="text/javascript" src="js/Terminal_Dosis_300.font.js"></script>
<script type="text/javascript" src="js/atooltip.jquery.js"></script>
<script src="js/roundabout.js" type="text/javascript"></script>
<script src="js/roundabout_shapes.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.2.js" type="text/javascript"></script>
<script type="text/javascript" src="js/script.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<style type="text/css">.bg {behavior:url("js/PIE.htc")}</style>
<![endif]-->
</head>
<body id="page6">
<div class="body1">
  <div class="body2">
    <div class="body3">
      <div class="main">
        <!-- header -->
        <header>
          <div class="wrapper">
            <h1><a href="index.php" id="logo"></a></h1>
            <form id="search" action="#" method="post">
            </form>
            <nav>
              <ul id="menu">
                <li id="<?php echo (($active =='home')?'active':'');?>"><a href="index.php">Home</a></li>
                <li id="<?php echo (($active =='about')?'active':'');?>"><a href="about.php">About</a></li>
                <li id="<?php echo (($active =='gallery')?'active':'');?>"><a href="folio.php">Gallery</a></li>
                <li id="<?php echo (($active =='register')?'active':'');?>" class="end"><a href="contact.php">Register</a></li>
              </ul>
            </nav>
          </div>
        </header>