<?php 
global $active;
$active = "upload";
include 'header2.php'; 
include 'global.php'; 
include 'dbcon.php';
if($_POST){
    $data['eid'] = $_POST['eid'];
    $data['attach'] = $_FILES["userfile"]['name'];

    if($_FILES["userfile"]['name']!=""){
        //uploading attached file
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["userfile"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["userfile"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png") {
            echo "Sorry, only PNG, JPG and JPEG files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $target_file)) {
                // echo "The file ". basename( $_FILES["attach"]["name"]). " has been uploaded.";
                insert_payment($data);
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }else{
        insert_payment($data);
    }
}
else{
    $eid = $_GET['eid'];
	$sql = "SELECT * FROM CM_entries WHERE e_id = ".$eid;
	$res = $conn->query($sql);
		$row = $res->fetch_array(MYSQLI_ASSOC);
		
		$idss = $row['e_category'];
	
	$sql2 = "SELECT * FROM CM_subject_category WHERE sc_id = ".$idss;
	$res2 = $conn->query($sql2);
		$row2 = $res2->fetch_array(MYSQLI_ASSOC);	
		
}


?>
            <!-- / header-->
            <!-- content -->
                <section id="content">
				<p><?=$row['e_presentation_date'];?></p>
				<p><?=$row2['sc_desc'];?></p>
                    <div class="wrapper">
					<div class="wrapper"> <span>Presentation Date:</span> </div>
                        <h2>Upload proof of payment here!</h2>
                        <form action="upload.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="eid" value="<?=$eid?>" >
            				<span>&nbsp;</span> <input id="userfile" name="userfile" type="file" style="display:none" accept=".jpg,.png,.jpeg"> 
            				<label for="userfile" class="button">Upload</label>
                            <input type="submit" class="button" name="submit" value="Submit">
                        </form>
        		    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="body4">
    <div class="main">
        <section id="content2">
            <div class="line2 wrapper">
            </div>
        </section>
    </div>
</div>
    <!-- / content -->
<?php include 'footer.php'; ?>