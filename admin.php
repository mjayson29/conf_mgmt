<?php  ?>
        <!-- / header-->
        <!-- content -->

<?php
require 'dbcon.php';
require 'global.php';
include 'header2.php';
session_start();
if(!isset($_SESSION['login_user'])){
    header("location: login.php");
}

    $sql = "SELECT * FROM CM_entries";
    if(isset($_GET['category'])||isset($_GET['status'])){
        $sql .= " WHERE ";
        if($_GET['category']!=''){
            $sql .= " e_category = ".$_GET['category']." AND";
        }
        if($_GET['status']!=''){
            $sql .= " e_status = ".$_GET['status'];
        }else{
            $sql = substr($sql, 0, -3);
        }
    }
    // echo $sql;
    $res = $conn->query($sql);
    

?>

<script type="text/javascript">
    
    $(document).ready(function(){
       
        $('#myTable').DataTable();

        $('#category').on('change', function(){
            $('#admin').submit();            
        });

        $('#status').on('change', function(){
            $('#admin').submit();                
        });

    });

</script>
                <section id="content">
                    <div class="wrapper">
                        <h2>Client Information</h2>
                        <div><label>Filter</label></div>
                        <form id="admin" action="admin.php" method="GET">
                            <div>
                                <span>Category</span>
                                <select id="category" name="category">
                                    <option value=""> < Select Category > </option>
                                <?php
                                $sql1 = "SELECT * FROM CM_subject_category";
                                $res1 = $conn->query($sql1);
                                if($res1->num_rows>0){
                                    while ($row1 = $res1->fetch_array(MYSQLI_ASSOC)) {
                                ?> 
                                    <option <?php echo ((isset($_GET['category']))?(($_GET['category']==$row1['sc_id'])?'selected':''):''); ?> value="<?=$row1['sc_id']?>"><?=$row1['sc_desc']?></option>
                                <?php }
                                }
                                 ?>
                                </select>
                            </div>
                            <div>
                                <span>Status</span>
                                <select id="status" name="status">
                                    <option value=""> < Select Status > </option>
                                    <option <?php echo ((isset($_GET['status']))?(($_GET['status']==1)?'selected':''):''); ?> value="1">New</option>
                                    <option <?php echo ((isset($_GET['status']))?(($_GET['status']==2)?'selected':''):''); ?> value="2">Accepted</option>
                                    <option <?php echo ((isset($_GET['status']))?(($_GET['status']==3)?'selected':''):''); ?> value="3">Rejected</option>
                                    <option <?php echo ((isset($_GET['status']))?(($_GET['status']==4)?'selected':''):''); ?> value="4">Paid</option>
                                </select>
                            </div>
                        </form>
                        <table id="myTable" class="display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Subject</th>
                                    <th>Title</th>
                                    <th>Presenter</th>
                                    <th>Institution</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            // print_r($res);
                            if($res!=""){
                                while ($row = $res->fetch_array(MYSQLI_ASSOC)) {
                                    switch ($row['e_status']) {
                                        case '1': $status = "New"; break;
                                        case '2': $status = "Accepted"; break;
                                        case '3': $status = "Rejected"; break;
                                        case '4': $status = "Paid"; break;
                                        case '5': $status = "Reserved"; break;
                                    }
                            ?>
                                <tr onclick="window.location.href='info.php?id=<?=$row['e_id']?>'">
                                    <td><?=str_pad($row['e_id'], 5, '0', STR_PAD_LEFT)?></td>
                                    <td><?=select_category($row['e_category'])?></td>
                                    <td><?=$row['e_title']?></td>
                                    <td><?=$row['e_name']?></td>
                                    <td><?=$row['e_school']?></td>
                                    <td><?=$status?></td>
                                </tr>
                            <?php
                                }
                            }else{
                                echo "<tr><td colspan='6'>No Entries</td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="body4">
    <div class="main">
        <section id="content2">
            <div class="line2 wrapper">
            </div>
        </section>
    </div>
</div>
<!-- / content -->
<?php include 'footer.php'; ?>