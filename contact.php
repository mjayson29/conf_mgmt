<?php 
global $active;
$active = 'register';
include 'header.php';
include 'dbcon.php';
include 'global.php';

if($_POST){
    $data['title'] = $_POST['title'];
    $data['presenter'] = $_POST['presenter'];
    $data['institution'] = $_POST['institution'];
    $data['email'] = $_POST['email'];
    $data['attach'] = $_FILES["attach"]['name'];

    if($_FILES["attach"]['name']!=""){
        //uploading attached file
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["attach"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["attach"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["attach"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "pdf" && $imageFileType != "doc" && $imageFileType != "docx") {
            echo "Sorry, only PDF, DOC and DOCX files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["attach"]["tmp_name"], $target_file)) {
                // echo "The file ". basename( $_FILES["attach"]["name"]). " has been uploaded.";
                insert_entries($data);
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }else{
        insert_entries($data);
    }

}

?>
        
                <section id="content">
                    <div class="wrapper">
                        <h2>Register</h2>
                        <form id="ContactForm" action="contact.php" method="POST" enctype="multipart/form-data">
                        <p>All Fields are required!</p>
                            <div>
                                <div class="wrapper"> <span>Title</span>
                                    <input id="title" name="title" type="text" class="input" >
                                </div>
                                <div class="wrapper"> <span>Presenter</span>
                                    <input id="presenter" name="presenter" type="text" class="input" >
                                </div>
                        	    <div class="wrapper"> <span>Institution</span>
                                    <input id="institution" name="institution" type="text" class="input" >
                                </div>
                                <div class="wrapper"> <span>Email</span>
                                    <input id="email" name="email" type="text" class="input" >
                                </div>
								<br>
                                <div class="wrapper"> <span>Abstract</span>
                        		    <input name="attach" type="file" id="attach" style="display:none">
                                    <label for="attach" class="button">Upload</label>
                        	   </div>
                        	   <!-- <span>&nbsp;</span> <a href="#" class="button">Cancel</a> <a href="#" class="button">Submit</a>  -->
                               <span>&nbsp;</span>
								<br>
                               <a href="#" class="button">Cancel</a>
                               <input type="submit" onclick="return valid(this)" class="button" name="" value="Submit">
                           </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="body4">
    <div class="main">
        <section id="content2">
            <div class="line2 wrapper">
                <div class="wrapper">
                    <article class="col1">
                        <h2>Miscellaneous</h2>
                        <div class="pad"> 
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est. 
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- / content -->
<?php include 'footer.php'; ?>

<script type="text/javascript">
    function valid(form){
        var var1 = $('#title').val();
        var var2 = $('#presenter').val();
        var var3 = $('#institution').val();
        var var4 = $('#email').val();
        var var5 = $('#attach').val();

        if(var1==''||var2==''||var3==''||var4==''||var5==''){
            alert('All fields are required. Please try again.');
            return false;
        }
    }
</script>