<?php 
global $active;
$active = 'gallery';
include 'header.php'; 
?>
<script type="text/javascript">
$(document).ready(function () {
    $('#myRoundabout').roundabout({
        shape: 'square',
        minScale: 0.93, // tiny!
        maxScale: 1, // tiny!
        easing: 'easeOutExpo',
        clickToFocus: 'true',
        focusBearing: '0',
        duration: 800,
        reflect: true
    });
    tabs.init();
    // for lightbox
    if ($("a[rel^='prettyPhoto']").length) {
        $(document).ready(function () {
            // prettyPhoto
            $("a[rel^='prettyPhoto']").prettyPhoto({
                theme: 'light_square'
            });
        });
    }
});
</script>
        <!-- content -->
        <section id="content">
          <div class="line1 wrapper">
            <div class="wrapper tabs">
              <article class="col1">
                <h2>Categories</h2>
                <div class="pad">
                  <ul class="nav">
                    <li class="selected"><a href="#Arts">Arts, Humanities and Philosophy</a></li>
                    <li><a href="#Business">Business, Finance and Management</a></li>
                    <li><a href="#Education">Basic and Teacher Education</a></li>
                    <li><a href="#Engineering">Engineering and Technology</a></li>
                    <li><a href="#Governance">Governance, Policy Studies and Social Sciences</a></li>
                    <li><a href="#Research">Institutional Research and Extension</a></li>
				    <li><a href="Technology">Information Technology, Computer and Computing</a></li>
					<li><a href="Science">Science and Mathematics</a></li>
                  </ul>
                </div>
              </article>
              <article class="col2 pad_left1 tab-content" id="Arts">
                <h2>Gallery</h2>
                <ul class="gallery">
                  <li> <a href="images/big_img1.jpg" class="lightbox-image" rel="prettyPhoto[group1]" > <img src="images/page3_img1.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img2.jpg" class="lightbox-image" rel="prettyPhoto[group1]" > <img src="images/page3_img2.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img3.jpg" class="lightbox-image" rel="prettyPhoto[group1]" > <img src="images/page3_img3.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img4.jpg" class="lightbox-image" rel="prettyPhoto[group1]" > <img src="images/page3_img4.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img5.jpg" class="lightbox-image" rel="prettyPhoto[group1]" > <img src="images/page3_img5.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img6.jpg" class="lightbox-image" rel="prettyPhoto[group1]" > <img src="images/page3_img6.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img7.jpg" class="lightbox-image" rel="prettyPhoto[group1]" > <img src="images/page3_img7.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img8.jpg" class="lightbox-image" rel="prettyPhoto[group1]" > <img src="images/page3_img8.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img9.jpg" class="lightbox-image" rel="prettyPhoto[group1]" > <img src="images/page3_img9.jpg" alt=""> </a> </li>
                </ul>
              </article>
              <article class="col2 pad_left1 tab-content" id="Business">
                <h2>Gallery</h2>
                <ul class="gallery">
                  <li> <a href="images/big_img7.jpg" class="lightbox-image" rel="prettyPhoto[group2]" > <img src="images/page3_img7.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img8.jpg" class="lightbox-image" rel="prettyPhoto[group2]" > <img src="images/page3_img8.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img9.jpg" class="lightbox-image" rel="prettyPhoto[group2]" > <img src="images/page3_img9.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img1.jpg" class="lightbox-image" rel="prettyPhoto[group2]" > <img src="images/page3_img1.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img2.jpg" class="lightbox-image" rel="prettyPhoto[group2]" > <img src="images/page3_img2.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img3.jpg" class="lightbox-image" rel="prettyPhoto[group2]" > <img src="images/page3_img3.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img4.jpg" class="lightbox-image" rel="prettyPhoto[group2]" > <img src="images/page3_img4.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img5.jpg" class="lightbox-image" rel="prettyPhoto[group2]" > <img src="images/page3_img5.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img6.jpg" class="lightbox-image" rel="prettyPhoto[group2]" > <img src="images/page3_img6.jpg" alt=""> </a> </li>
                </ul>
              </article>
              <article class="col2 pad_left1 tab-content" id="Education">
                <h2>Gallery</h2>
                <ul class="gallery">
                  <li> <a href="images/big_img4.jpg" class="lightbox-image" rel="prettyPhoto[group3]" > <img src="images/page3_img4.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img5.jpg" class="lightbox-image" rel="prettyPhoto[group3]" > <img src="images/page3_img5.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img6.jpg" class="lightbox-image" rel="prettyPhoto[group3]" > <img src="images/page3_img6.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img7.jpg" class="lightbox-image" rel="prettyPhoto[group3]" > <img src="images/page3_img7.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img8.jpg" class="lightbox-image" rel="prettyPhoto[group3]" > <img src="images/page3_img8.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img9.jpg" class="lightbox-image" rel="prettyPhoto[group3]" > <img src="images/page3_img9.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img1.jpg" class="lightbox-image" rel="prettyPhoto[group3]" > <img src="images/page3_img1.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img2.jpg" class="lightbox-image" rel="prettyPhoto[group3]" > <img src="images/page3_img2.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img3.jpg" class="lightbox-image" rel="prettyPhoto[group3]" > <img src="images/page3_img3.jpg" alt=""> </a> </li>
                </ul>
              </article>
              <article class="col2 pad_left1 tab-content" id="Engineering">
                <h2>Gallery</h2>
                <ul class="gallery">
                  <li> <a href="images/big_img1.jpg" class="lightbox-image" rel="prettyPhoto[group4]" > <img src="images/page3_img1.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img2.jpg" class="lightbox-image" rel="prettyPhoto[group4]" > <img src="images/page3_img2.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img3.jpg" class="lightbox-image" rel="prettyPhoto[group4]" > <img src="images/page3_img3.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img4.jpg" class="lightbox-image" rel="prettyPhoto[group4]" > <img src="images/page3_img4.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img5.jpg" class="lightbox-image" rel="prettyPhoto[group4]" > <img src="images/page3_img5.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img6.jpg" class="lightbox-image" rel="prettyPhoto[group4]" > <img src="images/page3_img6.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img7.jpg" class="lightbox-image" rel="prettyPhoto[group4]" > <img src="images/page3_img7.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img8.jpg" class="lightbox-image" rel="prettyPhoto[group4]" > <img src="images/page3_img8.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img9.jpg" class="lightbox-image" rel="prettyPhoto[group4]" > <img src="images/page3_img9.jpg" alt=""> </a> </li>
                </ul>
              </article>
              <article class="col2 pad_left1 tab-content" id="Governance">
                <h2>Gallery</h2>
                <ul class="gallery">
                  <li> <a href="images/big_img7.jpg" class="lightbox-image" rel="prettyPhoto[group5]" > <img src="images/page3_img7.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img8.jpg" class="lightbox-image" rel="prettyPhoto[group5]" > <img src="images/page3_img8.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img9.jpg" class="lightbox-image" rel="prettyPhoto[group5]" > <img src="images/page3_img9.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img1.jpg" class="lightbox-image" rel="prettyPhoto[group5]" > <img src="images/page3_img1.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img2.jpg" class="lightbox-image" rel="prettyPhoto[group5]" > <img src="images/page3_img2.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img3.jpg" class="lightbox-image" rel="prettyPhoto[group5]" > <img src="images/page3_img3.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img4.jpg" class="lightbox-image" rel="prettyPhoto[group5]" > <img src="images/page3_img4.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img5.jpg" class="lightbox-image" rel="prettyPhoto[group5]" > <img src="images/page3_img5.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img6.jpg" class="lightbox-image" rel="prettyPhoto[group5]" > <img src="images/page3_img6.jpg" alt=""> </a> </li>
                </ul>
              </article>
              <article class="col2 pad_left1 tab-content" id="Research">
                <h2>Gallery</h2>
                <ul class="gallery">
                  <li> <a href="images/big_img4.jpg" class="lightbox-image" rel="prettyPhoto[group6]" > <img src="images/page3_img4.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img5.jpg" class="lightbox-image" rel="prettyPhoto[group6]" > <img src="images/page3_img5.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img6.jpg" class="lightbox-image" rel="prettyPhoto[group6]" > <img src="images/page3_img6.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img7.jpg" class="lightbox-image" rel="prettyPhoto[group6]" > <img src="images/page3_img7.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img8.jpg" class="lightbox-image" rel="prettyPhoto[group6]" > <img src="images/page3_img8.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img9.jpg" class="lightbox-image" rel="prettyPhoto[group6]" > <img src="images/page3_img9.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img1.jpg" class="lightbox-image" rel="prettyPhoto[group6]" > <img src="images/page3_img1.jpg" alt=""> </a> </li>
                  <li> <a href="images/big_img2.jpg" class="lightbox-image" rel="prettyPhoto[group6]" > <img src="images/page3_img2.jpg" alt=""> </a> </li>
                  <li class="end"> <a href="images/big_img3.jpg" class="lightbox-image" rel="prettyPhoto[group6]" > <img src="images/page3_img3.jpg" alt=""> </a> </li>
                </ul>
              </article>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
<div class="body4">
  <div class="main">
    <section id="content2"> </section>
  </div>
</div>
<!-- / content -->
<?php include 'footer.php'; ?>